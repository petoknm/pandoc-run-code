# pandoc-run-code

Example:

    # Bash

    ```{.bash .run}
    echo $((1+2))
    ```

    # Python

    ```{.python .run}
    print(2^12)
    ```
