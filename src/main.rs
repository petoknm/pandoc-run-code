#![feature(bind_by_move_pattern_guards)]
extern crate pandoc_ast;
extern crate serde_json;
extern crate rayon;

use std::io::{self, Read};
use std::process::Command;

use pandoc_ast::Pandoc;
use pandoc_ast::Block::{self, *};
use serde_json::{from_str, to_string};
use rayon::prelude::*;

const HANDLERS: &[(&str, fn(&str) -> String)] = &[
    ("bash", bash),
    ("python", python)
];

fn main() {
    let mut input = String::new();
    let stdin = io::stdin();
    let mut handle = stdin.lock();
    handle.read_to_string(&mut input).unwrap();

    let mut pandoc: Pandoc = from_str(&input).unwrap();
    pandoc.blocks = pandoc.blocks.par_iter().map(transform).collect();
    println!("{}", to_string(&pandoc).unwrap());
}

fn transform(block: &Block) -> Block {
    if let CodeBlock((id, classes, kvs), code) = block.clone() {
        for handler in HANDLERS {
            if contains(&classes, handler.0) {
                let output = (handler.1)(&code);
                return CodeBlock((id, classes, kvs), output)
            }
        }
        return CodeBlock((id, classes, kvs), code);
    }
    block.clone()
}

fn contains(list: &[String], word: &str) -> bool {
    list.iter().any(|w| w == word)
}

fn bash(code: &str) -> String {
    let stripped_code = code.lines().map(|line|
        if line.starts_with('$') {
            &line[1..]
        } else {
            &line
        }
    ).collect::<Vec<_>>().join("\n");

    let output = String::from_utf8(
        Command::new("bash").args(&["-c", &stripped_code]).output().unwrap().stdout
    ).unwrap();

    let code = code.lines().map(|line|
        if line.starts_with('$') {
            format!("{}", line)
        } else {
            format!("$ {}", line)
        }
    ).collect::<Vec<_>>().join("\n");

    format!("{}\n{}", code, output)
}

fn python(code: &str) -> String {
    let output = String::from_utf8(
        Command::new("python").args(&["-c", code]).output().unwrap().stdout
    ).unwrap();

    format!("{}\n# Output\n{}", code, output)
}
